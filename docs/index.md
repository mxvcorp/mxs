# MXS - MXV Smart

---

## 1. Tổng quan

MXS Structure

├── [common](mxs-common/index.md)\ Thư viện chung

├── [mxs-share](mxs-share/index.md)\ Thư viện Share

├── [mxs-log](mxs-log/index.md)\ Service Activity Log

├── [mxs-cfg](mxs-cfg/index.md)\ Service Config

├── [mxs-auth](mxs-auth/index.md)\ Service Authentication

├── [mxs-api](mxs-api/index.md)\ API Gateway

├── [mxs-fe](mxs-fe/index.md)\ Frontend

---

## 2. Cài đặt môi trường

### Thiết lập môi trường Developers

- MONGO_URI: "mongodb://10.0.0.22/database_name"
- KAFKA_BROKERS: "10.0.196.21:9092"
- REDIS_URL / REDIS_PASS: "redis://10.0.196.11:6379" / "mxV@Dev.2o22"

### Cài đặt Thư viện common + share

Cài đặt vào các Service sử dụng cú pháp [npm-link](https://docs.npmjs.com/cli/v8/commands/npm-link){:target="\_blank"}

Ví dụ:

```
cd ~/mxs/mxs-common/build   # go into the package directory
npm link                    # creates global link (Checking globally installed packages: "npm list -g")

cd ~/mxs/mxs-share/build
npm link

cd ~/mxs/mxs-cfg   				        # go into some other package directory.
npm link @mxs/common @mxs/share	  # link-install the package
```

> Chú ý:
>
> - Link nhiều module thì viết trên cùng 1 câu lệnh
> - Khi cài đặt module mới trong project phải link lại

---

## 3. Diễn giải một số Thuật ngữ viết tắt

| Thuật ngữ/viết tắt | Giải thích |
| :----------------- | :--------: |
| BE                 |  Backend   |
| FE                 |  Frontend  |
| DB                 |  Database  |

---

## 4. Tham khảo

- Monolithic & [Microservices Architecture](https://learn.microsoft.com/en-us/azure/architecture/guide/architecture-styles/microservices){:target="\_blank"}
- Node.js [https://nodejs.org/en/](https://nodejs.org/en/){:target="\_blank"}
- TypeScript [https://www.typescriptlang.org/](https://www.typescriptlang.org/){:target="\_blank"}
- MongoDB [https://www.mongodb.com/](https://www.mongodb.com/){:target="\_blank"}
- Redis [https://redis.com/](https://redis.com/){:target="\_blank"}
- Kafka [https://kafka.apache.org/](https://kafka.apache.org/){:target="\_blank"}
- Jest (JavaScript Testing Framework) [https://jestjs.io/](https://jestjs.io/){:target="\_blank"}
- Robot Framework (Test) [https://github.com/robotframework](https://github.com/robotframework){:target="\_blank"}
  - [Robocorp-Recorder](https://github.com/robocorp/Robocorp-Recorder){:target="\_blank"} https://chrome.google.com/webstore/detail/robocorp-recorder/mcecdbgadanapabbhbejbkmmgcifcdbn
  - [Robotcorder](https://github.com/sohwendy/Robotcorder){:target="\_blank"} https://chrome.google.com/webstore/detail/robotcorder/ifiilbfgcemdapeibjfohnfpfmfblmpd

## 5. Công cụ sử dụng

### Công cụ sử dụng

- [VS Code ](https://code.visualstudio.com/download){:target="\_blank"}, VS Code Extentions:

  - Eslint: Tool phân tích code Javascript để xác định các đoạn code có lỗi (hoặc có khả năng có lỗi), có thể fix tự động
  - Prettier: Code formatter
  - Lint-staged
  - Husky
  - CommitLint

- Postman [https://www.postman.com/downloads/](https://www.postman.com/downloads/){:target="\_blank"}
- MongoDB Compass [https://www.mongodb.com/products/compass](https://www.mongodb.com/products/compass){:target="\_blank"}
- RedisInsight [https://redis.com/redis-enterprise/redis-insight/](https://redis.com/redis-enterprise/redis-insight/){:target="\_blank"}
- Git SCM [https://git-scm.com/downloads](https://git-scm.com/downloads){:target="\_blank"}
- Docker [https://www.docker.com/products/docker-desktop/](https://www.docker.com/products/docker-desktop/){:target="\_blank"}
