# mxs-common

---

## Authentication

- requireAuth: Kiểm tra Authentication

- requireRole: Kiểm tra Authorization

[(Phân biệt Authentication và Authorization)](https://viblo.asia/p/phan-biet-su-khac-nhau-giua-authentication-va-authorization-Eb85oad4Z2G){:target="\_blank"}

## Utilities

### Converter

- toUnixTime

### Encrypt / Decrypt

- Hash256
- SHA256
- encryptAES
- decryptAES
- removeAscent
- shuffleArray
- generateRandom
- generateHash
