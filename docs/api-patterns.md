# API Design Patterns for REST

---

## HTTP Methods and Status Codes

### HTTP Methods and Status Codes

For designing REST APIs, Each HTTP request includes a method, sometimes called “HTTP verbs,” that provides a lot of context for each call. Here’s a look at the most common HTTP methods:

- **GET**: read data from API
- **POST**: add new data to API
- **PUT**: update existing data with API
- **PATCH**: updates a subset of existing data with API
- **DELETE**: remove data (usually a single resource) from API

### Some common HTTP status codes include:

- **200**: Successful request, often a GET
- **201**: Successful request after a create, usually a POST
- **204**: Successful request with no content returned, usually a PUT or PATCH
- **301**: Permanently redirect to another endpoint
- **400**: Bad request (client should modify the request)
- **401**: Unauthorized, credentials not recognized
- **403**: Forbidden, credentials accepted but don’t have permission
- **404**: Not found, the resource does not exist
- **410**: Gone, the resource previously existed but does not now
- **429**: Too many requests, used for rate limiting and should include retry headers
- **500**: Server error, generic and worth looking at other 500-level errors instead
- **503**: Service unavailable, another where retry headers are useful

There are many more HTTP status codes and methods to consider, but the above lists should get you well on way for most APIs.

## Rest API Endpoint Formats

Basic CRUD functionality (Create, Read, Update, Delete) you would end up with the following routes:

- **[GET]** /api/user - Lists users
- **[GET]** /api/user/:id - Gets a user by ID
- **[POST]** /api/user - Creates a user
- **[PUT]** /api/user/:id - Updates a user by ID
- **[DELETE]** /api/user/:id - Deletes user by ID
- **[DELETE]** /api/user - Bulk delete user

In this example, you only actually have 2 routes "/api/user" and "/api/user/:id". If you were to just use POST, then you would end up with 6 different routes.

> As you're designing RESTful APIs, you’ll want to rely on the HTTP methods and best practices to express the primary purpose of a call. For that reason, you don’t want to use a POST to simply retrieve data. Nor would you want a GET to create or remove data. You should examine use cases to determine when to use each. Here are several factors that will influence decision.

> - GET requests can be cached
> - GET requests are idempotent (in that they can be called any number of times while guaranteeing the same outcome)
> - GET requests should never be used when dealing with sensitive data
> - GET requests have length restrictions
> - GET requests should be used only to retrieve data
> - POST requests are never cached (unless specified in the header)
> - POST requests are NOT idempotent
> - POST requests have no restrictions on data length

### Rest API Success Responses

1- GET - Get single item - HTTP Response Code: 200

```json

```

2- GET - Get item list - HTTP Response Code: 200

```json

```

3- POST - Create a new item - HTTP Response Code: 201

```json

```

4- PUT - Update an item - HTTP Response Code: 200/204

> If updated entity is to be sent after the update

```json

```

> If updated entity is not to be sent after the update

```json

```

5- DELETE - Delete an item - HTTP Response Code: 204

```json

```

### Rest API Error Responses

1- GET - HTTP Response Code: 404

```json

```

2- DELETE - HTTP Response Code: 404

```json

```

2- Bulk DELETE - HTTP Response Code: 404

```json

```

3- POST - HTTP Response Code: 400

```json

```

4- PUT - HTTP Response Code: 400/404

```json

```

5- VERB Unauthorized - HTTP Response Code: 401

```json

```

6- VERB Forbidden - HTTP Response Code: 403

```json

```

7- VERB Conflict - HTTP Response Code: 409

```json

```

8- VERB Too Many Requests - HTTP Response Code: 429

```json

```

9- VERB Internal Server Error - HTTP Response Code: 500

```json

```

10- VERB Service Unavailable - HTTP Response Code: 503

```json

```

### Validation Error Formats

Validation error formats can be different depending on requirements.

```json
HTTP/1.1  400
Content-Type: application/json

{
    "message": "Validation errors in request", /* skip or optional error message */
    "errors": [
        {
            "message": "Oops! The email is invalid",
        } ,
        {
            "message": "Oops! The phone number format is not correct",
        },
        {
            "message": "FirstName is required",
        }
    ]
}
```

## Common API Parameters

There are three common types of parameters to consider for API:

- **Filtering**: Return only results that match a filter by using field age as parameters. For example:

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GET /users?age=30

- **Pagination**: {}. Example:

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GET /users?page=3&results_per_page=20

- **Sorting**: {}. Example:

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GET /users?sort_by=first_name&order=asc

## Cache Data to Improve Performance

Caching partially eliminates unnecessary trips to the server. Returning data from local memory rather than sending a query for each new request can improve app’s performance. GET requests are cacheable by default, however, POST requests require you to specify the cache requirements in the header. Caching, however, can lead to stale data on the client’s browser. Here’s how you would deal with this in the POST header.

## Expires

In this case, you specify an absolute expiry time for the cached version. Anything beyond the time specified is considered stale and must be updated with the origin server. You can specify a time up to one year in the future.

## Cache-Control

When using this option, you can set specific directives such as max-age, must-revalidate or no-transform.

## Last Modified

With this option, you use the response header to determine caching requirements. The responses’ Date value specifies when the initial response was generated. The Last-Modified date specifies when the response associated with the resource was last changed. The Last-Modified date cannot be less than the Date value.
