# Quy trình quản lý công việc theo Task

- Team DEV quản lý công việc theo Task trên Planner theo địa chỉ [https://tasks.office.com/mxv.vn/en-US/Home/Planner/](https://tasks.office.com/mxv.vn/en-US/Home/Planner/)

- File mẫu [Sprint Backlog - Template.xlsx](./files/Sprint Backlog - Template.xlsx) quản lý công việc

---

## Tạo Task

Task bao gồm Task chính và các Sub-Task, việc tạo Task phải thỏa mãn các yêu cầu sau:

- Task phải được **đặt tên rõ ràng** để hình dung được công việc.
- Task cần **diễn giải rõ trong phần ghi chú** về nội dung cần làm cho các Sub-Task (cân nhắc diễn giải trực tiếp hoặc trong file Sprint Backlog - Template.xlsx).
- Task cần có **thời gian bắt đầu**, **estimate thời gian hoàn thành dự kiến**, **thời gian hoàn thành thực tế**.
- Các Sub-Task được **gán mã** để dễ cho việc test kiểm tra.

> Dev cần phân chia Task sao cho hợp lý, yêu cầu tối thiểu cho việc estimate thời gian hoàn thành là **tối thiểu 1 Task/1 Sub-Task một ngày**

## Hoàn thành Task

Yêu cầu cho viện hoàn thành Task/Sub-Task:

- Task khi đánh dấu hoàn thành cần có đủ các thông tin như bên trên: **thời gian bắt đầu**, **estimate thời gian hoàn thành dự kiến**, **thời gian hoàn thành thực tế**.
- Sub-Task khi đánh dấu hoàn thành cần **có 1 File Ảnh chụp kết quả tương ứng** và đẩy vào Attachments của Task (Ảnh hoàn thành Sub-Task đặt tên giống Sub-Task).
