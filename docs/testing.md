# Testing

Test gồm các hạng mục sau:

- Kiểm thử tự động.
- Kiểm tra chéo.
- Kiểm thử người dùng.
- Kiểm thử hoàn thành.

---

## Kiểm thử tự động (routes test)

Yêu cầu cơ bản cho việc kiểm thử tự động:

- Kiểm tra hợp lệ.
- Kiểm tra không hợp lệ.

### Thiết lập môi trường Test

src

├── **test**

├──├── env_setup.ts\ Thiết lập các biến môi trường Test

├──├── setup.ts\ Khởi tạo các tham số Test

├── **routes**

├──├── **\_\_test\_\_**

├──├──├── [action1].test.ts

├──├──├── [action2].test.ts

├──├──├── [action..].test.ts

---

Sử dụng cơ chế kiểm tra hồi quy [(Regression Testing)](https://viblo.asia/p/overview-ve-regression-testing-vyDZODkxlwj) để bất cứ khi nào có một sự thay đổi trong hệ thống, bao gồm:

- Bất cứ khi nào một tính năng mới được thực hiện, tính năng cũ được sửa đổi / thay đổi

- Giúp phát hiện các lỗi không mong muốn gây ra do những thay đổi mới được thêm vào hệ thống.

- Giảm số lượng lỗi cho người dùng cuối

### Kịch bản Test cơ bản

- Sử dụng global.signin đối với các Unit Test có check Auth
- Common CRUD Operations

| Action              | HTTP method | Relative URI     | Unit Test                                |
| :------------------ | :---------- | :--------------- | :--------------------------------------- |
| Get a list of all   | GET         | /api/endpoint/   |                                          |
| Get a item by ID    | GET         | /api/endpoint/id | {**empty_id** \| **null_item**}          |
| Create a item       | POST        | /api/endpoint/   | {**invalid_inputs** \| **exist_by_key**} |
| Update a item by ID | PUT         | /api/endpoint/id | {**empty_id** \| **null_item**}          |
| Delete a item by ID | DELETE      | /api/endpoint/id | {**empty_id** \| **null_item**}          |

Diễn giải:

- empty_id:
- null_item:
- invalid_inputs:
- exist_by_key:

## Convention testing
