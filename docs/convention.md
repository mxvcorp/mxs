# Quy chuẩn Thiết kế và Xây dựng hệ thống MXS

---

## 1. Cấu trúc Service Project

- routes:
- services:
- Đặt key cho mxs_configurations: Chuẩn hóa về quy tắc tránh đặt bừa hoặc trùng nhau

## 2. Naming Conventions

### Database

- Đặt tên Collections:
- Đặt tên Fields:

### Gitlab

- Đặt tên Projects:
- Đặt tên Branchs:
- Đặt tên Tags:
- Đặt tên Issues:
- Đặt tên Releases:

## 3. Gitlab

### Branch

### Merge

---

Trong lập trình quy chuẩn đầu tiên cần đưa ra là quy chuẩn cho việc đặt tên. Có 3 chuẩn để đặt tên là underscore, camelCase và PascalCase.

- underscore: sử dụng dấu gạch chân giữa các từ, tất cả các từ đều viết thường, ví dụ: $this_is_my_variable.
- camelCase: giống như cách viết của nó, từ đầu tiên viết thường, các từ tiếp theo viết hoa chữ cái đầu, ví dụ $thisIsMyVariable.
- PascalCase: viết hoa tất cả các chữ cái đầu, ví dụ $ThisIsMyVariable.

Một số quy chuẩn đặt tên thường dùng trong dự án [(tham khảo)](https://viblo.asia/p/ban-ve-quy-cach-dat-ten-naming-convention-3P0lPyem5ox):

- Tên lớp đặt theo PascalCase, ví dụ: UserClass, CategoryClass…
- Tên hàm và phương thức sử dụng camelCase, ví dụ getUser, getCategory…
- Tên biến cũng sử dụng camelCase loginUser,loginUser,categoryList…
- Tên hằng số thì đặc biệt, viết hoa hết và cách nhau bởi dấu gạch dưới DISCOUNT_PERCENT, LIMIT_RATE…
- Tên bảng, tên cột trong Database sử dụng underscore và sử dụng danh từ số nhiều, ví dụ bảng oauth_clients, oauth_refresh_tokens.
- Tên phần tử trong HTML, ví dụ khi bạn sử dụng Vue.js, React… tạo ra thì nó sẽ có dạng KebabCase, ví dụ <my-component>.
