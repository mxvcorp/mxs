# MXS Microservice

MXS
├── common\ Thư viện chung
├── mxs-share\ Thư viện Share
├── mxs-log\ Service Logs
├── mxs-cfg\ Service Cấu hình
├── mxs-auth\ Service Phân quyền
├── mxs-api\ Api Gateway
├── mxs-fe\ Frontend
└── README.md The main readme\

## Cài đặt môi trường

- Thư viện common + share vào chung tất cả các Project sử dụng [npm-link](https://docs.npmjs.com/cli/v8/commands/npm-link):

Ví dụ:

```
cd ~/mxs/mxs-common				# go into the package directory
npm link                		# creates global link (Checking globally installed packages: "npm list -g")

cd ~/mxs/mxs-share
npm link

cd ~/mxs/mxs-cfg   				# go into some other package directory.
npm link @mxs/common @mxs/share	# link-install the package
```

Chú ý link nhiều module thì viết trên cùng 1 câu lệnh

# README

## Reference

- [pm2](https://pm2.keymetrics.io/docs/usage/process-management/)
- [KafkaJS](https://kafka.js.org/docs/introduction)
- [node-rdkafka](https://github.com/Blizzard/node-rdkafka#readme)

- [mxv-devcore](https://mxvcorp.gitlab.io/devcore/)

## License

MXV Software Research and Development.
